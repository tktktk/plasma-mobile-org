---
menu:
  main:
    name: Installer
    weight: 4
sassFiles:
- scss/get.scss
title: Distributions proposant Plasma Mobile
---
## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM est une distribution Manjaro mais pour périphériques ARM. Elle repose sur Arch Linux ARM, associée à des outils, des thèmes et de l'infrastructure de Manjaro pour réaliser des installations pour votre périphérique ARM.

[Site Internet](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Télécharger :

* [Dernière version stable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Compilations pour développeurs (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

### OpenSUSE

![](/img/openSUSE.svg)

OpenSUSE, anciennement SUSE Linux et SuSE Linux Professionnal, est un distribution Linux, développée par SUS Linux GmbH et d'autres entreprises. Actuellement, OpenSUSE fournit Tumbleweed, reposant sur les compilations de Plasma Mobile.

##### Télécharger

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) est une distribution Linux Alpine, pré configurée et optimisée pour périphériques tactiles, pouvant être installée sur les téléphones portable et d'autres périphériques mobiles. Vous pouvez afficher une [liste de périphériques](https://wiki.postmarketos.org/wiki/Devices) pour voir l'avancement de la prise en charge de votre périphérique.

Pour les appareils n'ayant pas d'images pré construites, vous devrez le flasher manuellement en utilisant l'utilitaire « pmbootstrap ». Veuillez suivre les instructions [ici] (https://wiki.postmarketos.org/wiki/Installation_guide). Veuillez vérifiez également la page wiki de l'appareil pour plus d'informations sur ce qui fonctionne.

[En savoir plus](https://postmarketos.org)

##### Télécharger :

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Version la plus récente (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Périphériques de la communauté](https://postmarketos.org/download/)

### Référence à « rootfs » reposant sur Neon

![](/img/neon.svg)

L'image est construite à partir de KDE Neon. KDE Neon, lui même, est construit à partir de Ubuntu 20.04 (Focal). Cette image repose sur une branche de développement non stable de KDE Neon. Elle embarque toujours les dernières versions des environnements de développement de KDE, KWin et Plasma Mobile, compilées à partir de la branche « git master ».

Il n'y a actuellement aucun travail en cours pour la maintenance des images.

##### Télécharger :

* [PinePhone](https://images.plasma-mobile.org/pinephone/)

## Installation

Téléchargez l'image, décompressez la et enregistrez la sur une carte « SD » en utilisant « dd » ou un outil graphique. Le PinePhone démarrera automatiquement à partir de la carte « SD ». Pour l'installer vers la mémoire intégrée, veuillez suivre les instructions du [Wiki de Pine](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Périphériques de bureau

### Image « ISO » reposant sur Neon en « amd 64 »

![](/img/neon.svg)

L'image « ISO » utilise les mêmes paquets que la référence « rootfs » reposant sur Neon, simplement compilé pour « amd64 ». Elle peut être testée sur des tablettes Intel non Android, des ordinateurs de bureau et des machines virtuelles.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
