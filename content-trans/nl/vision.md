---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Visie
    parent: project
    weight: 1
title: Onze visie
---
Plasma Mobile heeft als doel een volledig en open softwaresysteem te worden voor mobiele apparaten.<br /> Het is ontworpen om gebruikers die privacy in het oog hebben de controle terug te geven over hun informatie en communicatie.

Plasma Mobile volgt een pragmatische benadering en is inclusief richting software van derden, biedt de gebruiker de keuze welke toepassingen en services te gebruiken, tegelijk met leveren van een naadloze ervaring over meerdere apparaten.<br /> Plasma Mobile implementeert open standaarden en is ontwikkeld in een transparent proces dat open is voor iedereen om in deel te nemen.
