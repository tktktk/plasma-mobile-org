---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Vision
    parent: project
    weight: 1
title: Vår vision
---
Plasma Mobil har som mål att bli ett fullständigt och öppet programvarusystem för mobila apparater.<br /> Det är konstruerat för att ge integritetsmedvetna användare tillbaka kontrollen över deras information och kommunikation.

Plasma Mobil är pragmatiskt och inkluderar tredjepartsprogramvara, vilket låter användaren välja vilka program och tjänster som ska användas, medan det tillhandahåller en sömlös upplevelse över flera apparater.<br /> Plasma Mobil implementerar öppna standarder och utvecklas i en transparent process som är öppen för vem som helst att delta i.
