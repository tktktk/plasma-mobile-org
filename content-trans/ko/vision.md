---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: 비전
    parent: project
    weight: 1
title: 우리의 비전
---
Plasma 모바일의 목표는 모바일 장치를 위한 완전한 오픈 소스 시스템 개발입니다.<br />프라이버시를 중시하는 사용자가 개인 정보와 통신의 통제권을 되찾을 수 있도록 돕습니다.

Plasma 모바일은 실용적인 접근을 택하고 제3자 소프트웨어에 개방적입니다. 사용자는 자기가 사용할 프로그램과 서비스를 원하는 대로 결정할 수 있으며, 여러 장치에서 통합적으로 지원합니다.<br />Plasma 모바일은 공개 표준을 구현하며 누구나 참여할 수 있도록 투명하게 개발됩니다.
